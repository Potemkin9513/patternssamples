﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatternsSamples.Builder
{
    // Product
    internal sealed class Bread
    {
        internal Salt Salt { get; set; }
        internal Flour Flour { get; set; }
        internal Additives Additives { get; set; }

        public override string ToString()
        {
            // write some bread description
            return string.Empty;
        }
    }
}
