﻿using PatternsSamples.Builder.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatternsSamples.Builder
{
    // Concrete Builder
    internal sealed class RyeBreadBuilder : BreadBuilder
    {
        internal override void SetAdditives()
        {
        }

        internal override void SetFlour()
        {
            this.Bread.Flour = new Flour { Sort = "Rzhanaya" };
        }

        internal override void SetSalt()
        {
            Bread.Salt = new Salt();
        }
    }
}
