﻿using PatternsSamples.Builder.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatternsSamples.Builder
{
    internal sealed class BuilderRunner : SampleRunner
    {
        internal override string PatternName { get; } = "Builder";

        internal override void Run()
        {
            BreadBuilder breadBuilder = new WheatBreadBuilder();
            Baker baker = new Baker();
            Bread bread = baker.Bake(breadBuilder);

            breadBuilder = new RyeBreadBuilder();
            bread = baker.Bake(breadBuilder);
        }
    }
}
