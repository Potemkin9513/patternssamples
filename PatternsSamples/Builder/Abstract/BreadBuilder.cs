﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatternsSamples.Builder.Abstract
{
    // Builder
    internal abstract class BreadBuilder
    {
        internal Bread Bread { get; set; }

        internal void CreateBread()
        {
            Bread = new Bread();
        }

        internal abstract void SetSalt();
        internal abstract void SetFlour();
        internal abstract void SetAdditives();
    }
}
