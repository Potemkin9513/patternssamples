﻿using PatternsSamples.Builder.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatternsSamples.Builder
{
    // Concrete Builder
    internal sealed class WheatBreadBuilder : BreadBuilder
    {
        internal override void SetAdditives()
        {
            Bread.Additives = new Additives() { Name = "MyAdditives" };
        }

        internal override void SetFlour()
        {
            Bread.Flour = new Flour() { Sort = "Pshenichnaya:)" };
        }

        internal override void SetSalt()
        {
            Bread.Salt = new Salt();
        }
    }
}
