﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatternsSamples.Builder
{
    internal sealed class Flour
    {
        internal string Sort { get; set; }
    }
}
