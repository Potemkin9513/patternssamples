﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatternsSamples.Composite.Abstract
{
    internal abstract class Component
    {
        protected string _name;

        public Component(string name)
        {
            _name = name;
        }

        internal abstract void Print();
    }
}
