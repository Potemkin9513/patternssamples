﻿using PatternsSamples.Composite.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatternsSamples.Composite
{
    internal class Directory : Component
    {
        private List<Component> _childComponents = new List<Component>();

        public Directory(string name) : base(name) { }

        internal void Add(Component component)
        {
            _childComponents.Add(component);
        }

        internal void Remove(Component component)
        {
            _childComponents.Remove(component);
        }

        internal override void Print()
        {
            MessageService.Send("Node " + _name);
            MessageService.Send("Sub-nodes:");
            for (int i = 0; i < _childComponents.Count; i++) {
                _childComponents[i].Print();
            }
        }
    }
}
