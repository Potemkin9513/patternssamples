﻿using PatternsSamples.Composite.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatternsSamples.Composite
{
    internal sealed class CompositeRunner : SampleRunner
    {
        internal override string PatternName => "Composite";

        internal override void Run()
        {
            Directory diskC = new Directory("Disk C");
            Directory documentsDirectory = new Directory("My Documents");
            Component image = new File("image.jpg");
            Component file = new File("file.txt");

            documentsDirectory.Add(image);
            documentsDirectory.Add(file);
            diskC.Add(documentsDirectory);

            diskC.Print();
        }
    }
}
