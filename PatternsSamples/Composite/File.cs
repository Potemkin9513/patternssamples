﻿using PatternsSamples.Composite.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatternsSamples.Composite
{
    internal sealed class File : Component
    {
        public File(string name) : base(name) { }

        internal override void Print() => MessageService.Send(_name);
    }
}
