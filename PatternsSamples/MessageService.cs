﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatternsSamples
{
    internal static class MessageService
    {
        internal static void Send(string message)
        {
            Console.WriteLine(message);
        }
    }
}
