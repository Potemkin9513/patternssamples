﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatternsSamples.AbstractFactory.Abstract
{
    internal abstract class Weapon
    {
        // Abstract Product
        internal abstract void Hit();
    }
}
