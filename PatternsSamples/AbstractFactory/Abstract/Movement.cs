﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatternsSamples.AbstractFactory.Abstract
{
    // Abstract Product
    internal abstract class Movement
    {
        internal abstract void Move();
    }
}
