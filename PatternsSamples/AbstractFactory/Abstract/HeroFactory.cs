﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatternsSamples.AbstractFactory.Abstract
{
    // Abstract Factory
    internal abstract class HeroFactory
    {
        internal abstract Movement CreateMovement();
        internal abstract Weapon CreateWeapon();
    }
}
