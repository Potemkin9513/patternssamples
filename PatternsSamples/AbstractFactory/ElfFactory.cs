﻿using PatternsSamples.AbstractFactory.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatternsSamples.AbstractFactory
{
    // Concrete Factory
    internal sealed class ElfFactory : HeroFactory
    {
        internal override Movement CreateMovement()
        {
            return new FlyMovement();
        }

        internal override Weapon CreateWeapon()
        {
            return new Сrossbow();
        }
    }
}
