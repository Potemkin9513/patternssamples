﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatternsSamples.AbstractFactory
{
    internal sealed class AbstractFactoryRunner : SampleRunner
    {
        internal override string PatternName { get; } = "Abstract Factory";

        internal override void Run()
        {
            Hero elf = new Hero(new ElfFactory());
            elf.Hit();
            elf.Move();

            Hero voin = new Hero(new WarriorFactory());
            voin.Hit();
            voin.Move();
        }
    }
}
