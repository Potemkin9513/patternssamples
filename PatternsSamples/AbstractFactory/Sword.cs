﻿using PatternsSamples.AbstractFactory.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatternsSamples.AbstractFactory
{
    internal sealed class Sword : Weapon
    {
        internal override void Hit()
        {
            MessageService.Send("Hit by sword");
        }
    }
}
