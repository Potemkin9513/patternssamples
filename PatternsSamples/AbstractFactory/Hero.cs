﻿using PatternsSamples.AbstractFactory.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatternsSamples.AbstractFactory
{
    // Client
    internal sealed class Hero
    {
        private Weapon _weapon;
        private Movement _movement;

        internal Hero(HeroFactory heroFactory)
        {
            _weapon = heroFactory.CreateWeapon();
            _movement = heroFactory.CreateMovement();
        }

        internal void Move()
        {
            _movement.Move();
        }

        internal void Hit()
        {
            _weapon.Hit();
        }
    }
}
