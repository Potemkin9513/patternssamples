﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatternsSamples.ResponsibilityChain.Abstract
{
    internal abstract class Handler
    {
        internal Handler Successor { get; set; }
        internal abstract void Handle(Receiver receiver);
    }
}
