﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatternsSamples.ResponsibilityChain
{
    internal sealed class Receiver
    {
        internal bool IsBankTransfer { get; private set; }
        internal bool IsMoneyTransfer { get; private set; }
        internal bool IsPayPalTransfer { get; private set; }

        public Receiver(bool isBankTransfer, bool isMoneyTransfer, bool isPayPalTransfer)
        {
            IsBankTransfer = isBankTransfer;
            IsMoneyTransfer = isMoneyTransfer;
            IsPayPalTransfer = isPayPalTransfer;
        }
    }
}
