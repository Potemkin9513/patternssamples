﻿using PatternsSamples.ResponsibilityChain.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatternsSamples.ResponsibilityChain
{
    internal sealed class PayPalPaymentHandler : Handler
    {
        internal override void Handle(Receiver receiver)
        {
            if (receiver.IsPayPalTransfer) {
                MessageService.Send("Request handled by PayPalPaymentHandler");
            } else {
                MessageService.Send("Request throwed to next by PayPalPaymentHandler");
                Successor.Handle(receiver);
            }
        }
    }
}
