﻿using PatternsSamples.ResponsibilityChain.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatternsSamples.ResponsibilityChain
{
    internal sealed class BankPaymentHandler : Handler
    {
        internal override void Handle(Receiver receiver)
        {
            if (receiver.IsBankTransfer) {
                MessageService.Send("Request handled by BankPaymentHandler");
            } else {
                MessageService.Send("Request throwed to next by BankPaymentHandler");
                Successor.Handle(receiver);
            }
        }
    }
}
