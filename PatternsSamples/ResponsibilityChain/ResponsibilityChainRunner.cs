﻿using PatternsSamples.ResponsibilityChain.Abstract;

namespace PatternsSamples.ResponsibilityChain
{
    internal sealed class ResponsibilityChainRunner : SampleRunner
    {
        internal override string PatternName => "Chain of responsibility";

        internal override void Run()
        {
            Receiver receiver = 
                new Receiver(isBankTransfer: false, isMoneyTransfer: true, isPayPalTransfer: true);

            Handler bankPaymentHandler = new BankPaymentHandler();
            Handler moneyPaymentHandler = new MoneyPaymentHandler();
            Handler payPalPaymentHandler = new PayPalPaymentHandler();

            bankPaymentHandler.Successor = moneyPaymentHandler;
            moneyPaymentHandler.Successor = payPalPaymentHandler;

            bankPaymentHandler.Handle(receiver);
        }
    }
}
