﻿using PatternsSamples.ResponsibilityChain.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatternsSamples.ResponsibilityChain
{
    internal sealed class MoneyPaymentHandler : Handler
    {
        internal override void Handle(Receiver receiver)
        {
            if (receiver.IsMoneyTransfer) {
                MessageService.Send("Request handled by MoneyPaymentHandler");
            } else {
                MessageService.Send("Request throwed to next by MoneyPaymentHandler");
                Successor.Handle(receiver);
            }
        }
    }
}
