﻿using PatternsSamples.Mediator.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatternsSamples.Mediator
{
    internal class Tester : Collegue
    {
        internal Tester(Abstract.Mediator mediator) 
            : base(mediator) { }

        internal override void Notify(string message)
        {
            MessageService.Send("Message to the tester: " + message);
        }
    }
}
