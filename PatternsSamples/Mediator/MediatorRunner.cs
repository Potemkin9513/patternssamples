﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatternsSamples.Mediator
{
    internal class MediatorRunner : SampleRunner
    {
        internal override string PatternName { get; } = "Mediator";

        internal override void Run()
        {
            ManagerMediator mediator = new ManagerMediator();

            Customer customer = new Customer(mediator);
            Programmer programmer = new Programmer(mediator);
            Tester tester = new Tester(mediator);

            mediator.Customer = customer;
            mediator.Programmer = programmer;
            mediator.Tester = tester;

            customer.Send("There is an order, you need to make a program");
            programmer.Send("The program is ready, it is necessary to test");
            tester.Send("The program is tested and ready for sale");
        }
    }
}
