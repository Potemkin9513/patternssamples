﻿using PatternsSamples.Mediator.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatternsSamples.Mediator
{
    internal class ManagerMediator : Abstract.Mediator
    {
        internal Customer Customer { get; set; }
        internal Programmer Programmer { get; set; }
        internal Tester Tester { get; set; }

        internal override void Send(string message, Collegue sender)
        {
            string recieverIsNullMessage = "Reciever is null!";
            if (sender is Customer) {
                if (Programmer == null) {
                    throw new Exception(recieverIsNullMessage);
                }
                Programmer.Notify(message);
            } else if (sender is Programmer) {
                if (Tester == null) {
                    throw new Exception(recieverIsNullMessage);
                }
                Tester.Notify(message);
            } else {
                if (Customer == null) {
                    throw new Exception(recieverIsNullMessage);
                }
                Customer.Notify(message);
            }
        }
    }
}
