﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatternsSamples.Mediator.Abstract
{
    internal abstract class Collegue
    {
        protected Mediator _mediator;

        internal Collegue(Mediator mediator)
        {
            _mediator = mediator;
        }

        internal void Send(string message)
        {
            _mediator.Send(message, this);
        }

        internal abstract void Notify(string message);
    }
}
