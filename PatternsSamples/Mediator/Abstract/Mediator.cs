﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatternsSamples.Mediator.Abstract
{
    internal abstract class Mediator
    {
        internal abstract void Send(string message, Collegue collegue);
    }
}
