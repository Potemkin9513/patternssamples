﻿using PatternsSamples.Mediator.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatternsSamples.Mediator
{
    internal class Programmer : Collegue
    {
        internal Programmer(Abstract.Mediator mediator) 
            : base(mediator) { }

        internal override void Notify(string message)
        {
            MessageService.Send("Message to the programmer: " + message);
        }
    }
}
