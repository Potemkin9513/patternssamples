﻿using PatternsSamples.Decorator.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatternsSamples.Decorator
{
    internal class BulgarianPizza : Pizza
    {
        public override string Name { get; protected set; } = "Bulgarian pizza";

        internal override decimal Cost => 150;
    }
}
