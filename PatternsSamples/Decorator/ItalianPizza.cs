﻿using PatternsSamples.Decorator.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatternsSamples.Decorator
{
    internal class ItalianPizza : Pizza
    {
        public override string Name { get; protected set; } = "Italian pizza";

        internal override decimal Cost => 100;
    }
}
