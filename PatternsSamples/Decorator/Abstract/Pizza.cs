﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatternsSamples.Decorator.Abstract
{
    internal abstract class Pizza
    {
        internal abstract Decimal Cost { get; }
        public abstract String Name { get; protected set; }
    }
}
