﻿using PatternsSamples.Decorator.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatternsSamples.Decorator
{
    internal class CheesePizza : PizzaDecorator
    {
        internal override decimal Cost => _pizza.Cost + 15;

        public override string Name { get ; protected set ; }

        public CheesePizza(Pizza pizza) : base(pizza)
        {
            Name = _pizza.Name + " + cheese";
        }
    }
}
