﻿using PatternsSamples.Decorator.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatternsSamples.Decorator
{
    internal class DecoratorRunner : SampleRunner
    {
        internal override string PatternName => "Decorator";

        internal override void Run()
        {
            Pizza pizza = new ItalianPizza();
            pizza = new TomatoPizza(pizza);
            pizza = new CheesePizza(pizza);

            MessageService.Send(pizza.Name);

            pizza = new BulgarianPizza();
            pizza = new CheesePizza(pizza);

            MessageService.Send(pizza.Name);
        }
    }
}
