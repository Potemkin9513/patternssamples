﻿using PatternsSamples.Decorator.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatternsSamples.Decorator
{
    internal class TomatoPizza : PizzaDecorator
    {
        internal override decimal Cost => _pizza.Cost + 10;

        public override string Name { get ; protected set ; }

        public TomatoPizza(Pizza pizza) : base(pizza)
        {
            Name = _pizza.Name + " + tomato";
        }
    }
}
