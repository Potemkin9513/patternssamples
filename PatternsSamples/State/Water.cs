﻿using PatternsSamples.State.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatternsSamples.State
{
    internal class Water
    {
        internal IWaterState WaterState { get; set; }

        public Water(IWaterState waterState)
        {
            WaterState = waterState;
        }

        internal void Heat()
        {
            WaterState.Heat(this);
        }

        internal void Frost()
        {
            WaterState.Frost(this);
        }
    }
}
