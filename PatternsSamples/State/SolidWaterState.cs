﻿using PatternsSamples.State.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatternsSamples.State
{
    internal class SolidWaterState : IWaterState
    {
        public void Frost(Water water)
        {
            MessageService.Send("Solid -> More cold solid");
        }

        public void Heat(Water water)
        {
            MessageService.Send("Solid -> Liquid");
            water.WaterState = new LiquidWaterState();
        }
    }
}
