﻿using PatternsSamples.State.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatternsSamples.State
{
    internal class GasWaterState : IWaterState
    {
        public void Frost(Water water)
        {
            MessageService.Send("Gas -> Liquid");
            water.WaterState = new LiquidWaterState();
        }

        public void Heat(Water water)
        {
            MessageService.Send("Gas -> More hot gas");
        }
    }
}
