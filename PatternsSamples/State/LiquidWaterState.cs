﻿using PatternsSamples.State.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatternsSamples.State
{
    internal class LiquidWaterState : IWaterState
    {
        public void Frost(Water water)
        {
            MessageService.Send("Liquid -> Solid");
            water.WaterState = new SolidWaterState();
        }

        public void Heat(Water water)
        {
            MessageService.Send("Liquid -> Gas");
            water.WaterState = new GasWaterState();
        }
    }
}
