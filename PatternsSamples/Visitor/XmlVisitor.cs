﻿using PatternsSamples.Visitor.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatternsSamples.Visitor
{
    internal class XmlVisitor : IVisitor
    {
        public void VisitCompanyAccount(Company account)
        {
            // Represents company in xml view
            MessageService.Send("Represents company in xml view");
        }

        public void VisitPersonAccount(Person account)
        {
            // Represents person in xml view
            MessageService.Send("Represents person in xml view");
        }
    }
}
