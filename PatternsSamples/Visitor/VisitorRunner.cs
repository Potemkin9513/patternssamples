﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatternsSamples.Visitor
{
    internal class VisitorRunner : SampleRunner
    {
        internal override string PatternName => "Visitor";

        internal override void Run()
        {
            Bank bank = new Bank();
            bank.AddAccount(new Person());
            bank.AddAccount(new Company());

            bank.Accept(new HtmlVisitor());
            bank.Accept(new XmlVisitor());
        }
    }
}
