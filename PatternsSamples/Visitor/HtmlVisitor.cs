﻿using PatternsSamples.Visitor.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatternsSamples.Visitor
{
    internal class HtmlVisitor : IVisitor
    {
        public void VisitCompanyAccount(Company account)
        {
            // Represents company in html view
            MessageService.Send("Represents company in html view");
        }

        public void VisitPersonAccount(Person account)
        {
            // Represents person in html view
            MessageService.Send("Represents person in html view");
        }
    }
}
