﻿using PatternsSamples.Visitor.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatternsSamples.Visitor
{
    internal class Person : IAccount
    {
        public void Accept(IVisitor visitor)
        {
            visitor.VisitPersonAccount(this);
        }
    }
}
