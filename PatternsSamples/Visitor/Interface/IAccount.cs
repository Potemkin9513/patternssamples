﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatternsSamples.Visitor.Interface
{
    internal interface IAccount
    {
        void Accept(IVisitor visitor);
    }
}
