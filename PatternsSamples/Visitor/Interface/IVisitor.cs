﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatternsSamples.Visitor.Interface
{
    internal interface IVisitor
    {
        void VisitPersonAccount(Person account);
        void VisitCompanyAccount(Company account);
    }
}
