﻿using PatternsSamples.Visitor.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatternsSamples.Visitor
{
    internal class Bank
    {
        List<IAccount> accounts = new List<IAccount>();

        internal void AddAccount(IAccount account)
        {
            accounts.Add(account);
        }

        internal void RemoveAccount(IAccount account)
        {
            accounts.Remove(account);
        }

        internal void Accept(IVisitor visitor)
        {
            foreach (var account in accounts) {
                account.Accept(visitor);
            }
        }

    }
}
