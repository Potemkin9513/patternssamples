﻿using System;

namespace PatternsSamples
{
    class Program
    {
        static void Main(string[] args)
        {
            SampleRunner[] sampleRunners = new SampleRunner[] {
                new Visitor.VisitorRunner(),
                new Adapter.AdapterRunner(),
                new State.StateRunner(),
                new ResponsibilityChain.ResponsibilityChainRunner(),
                new Composite.CompositeRunner()
            };

            RunPatternSamples(ref sampleRunners);

            Console.ReadKey();
        }

        private static void RunPatternSamples(ref SampleRunner[] sampleRunners)
        {
            foreach (var sample in sampleRunners) {
                Console.WriteLine(String.Concat(
                        "---------", 
                        Environment.NewLine,
                        sample.PatternName, 
                        Environment.NewLine)
                    );
                sample.Run();
            }
        }
    }
}
