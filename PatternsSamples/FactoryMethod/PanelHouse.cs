﻿using PatternsSamples.FactoryMethod.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatternsSamples.FactoryMethod
{
    // Concrete Product
    internal class PanelHouse : House
    {
        internal PanelHouse()
        {
            Console.WriteLine("Панельный дом построен");
        }
    }
}
