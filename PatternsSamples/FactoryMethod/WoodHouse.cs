﻿using PatternsSamples.FactoryMethod.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatternsSamples.FactoryMethod
{
    // Concrete Product
    internal sealed class WoodHouse : House
    {
        internal WoodHouse()
        {
            Console.WriteLine("Деревянный дом построен");
        }
    }
}
