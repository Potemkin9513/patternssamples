﻿using PatternsSamples.FactoryMethod.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatternsSamples.FactoryMethod
{
    internal sealed class FactoryMethodRunner : SampleRunner
    {
        internal override string PatternName { get; } = "Factory Method";

        internal override void Run()
        {
            Developer dev = new PanelDeveloper("ООО КирпичСтрой");
            House house2 = dev.Create();

            dev = new WoodDeveloper("Частный застройщик");
            House house = dev.Create();
        }
    }
}
