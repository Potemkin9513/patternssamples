﻿using PatternsSamples.FactoryMethod.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatternsSamples.FactoryMethod
{
    // Concrete Creator
    internal sealed class PanelDeveloper : Developer
    {
        internal PanelDeveloper(string n) : base(n)
        { }

        internal override House Create()
        {
            return new PanelHouse();
        }
    }
}
