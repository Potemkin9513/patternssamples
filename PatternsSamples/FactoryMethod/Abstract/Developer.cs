﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatternsSamples.FactoryMethod.Abstract
{
    // Abstract Creator
    internal abstract class Developer
    {
        internal string Name { get; set; }

        internal Developer(string n)
        {
            Name = n;
        }
        // фабричный метод
        internal abstract House Create();
    }
}
