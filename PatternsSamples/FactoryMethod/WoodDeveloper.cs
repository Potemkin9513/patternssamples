﻿using PatternsSamples.FactoryMethod.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatternsSamples.FactoryMethod
{
    // Concrete Creator
    internal sealed class WoodDeveloper : Developer
    {
        internal WoodDeveloper(string n) : base(n)
        { }

        internal override House Create()
        {
            return new WoodHouse();
        }
    }
}
