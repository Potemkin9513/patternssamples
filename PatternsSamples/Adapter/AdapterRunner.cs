﻿using PatternsSamples.Adapter.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatternsSamples.Adapter
{
    internal class AdapterRunner : SampleRunner
    {
        internal override string PatternName => "Adapter";

        internal override void Run()
        {
            Traveler traveler = new Traveler();
            Car car = new Car();
            traveler.Travel(car);

            IAnimal camel = new Camel();
            ITransport camelTransport = new AnimalToTransportAdapter(camel);

            traveler.Travel(camelTransport);
        }
    }
}
