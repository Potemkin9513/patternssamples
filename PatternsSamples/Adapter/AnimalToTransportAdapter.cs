﻿using PatternsSamples.Adapter.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatternsSamples.Adapter
{
    internal class AnimalToTransportAdapter : ITransport
    {
        private IAnimal _animal;

        public AnimalToTransportAdapter(IAnimal animal)
        {
            _animal = animal;
        }

        public void Drive()
        {
            _animal.Run();
        }
    }
}
