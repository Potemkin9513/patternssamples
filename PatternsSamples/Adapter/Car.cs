﻿using PatternsSamples.Adapter.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatternsSamples.Adapter
{
    internal class Car : ITransport
    {
        public void Drive()
        {
            MessageService.Send("The car went");
        }
    }
}
