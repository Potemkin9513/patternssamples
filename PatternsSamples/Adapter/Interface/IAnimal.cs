﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatternsSamples.Adapter.Interface
{
    internal interface IAnimal
    {
        void Run();
    }
}
