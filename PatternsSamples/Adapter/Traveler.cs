﻿using PatternsSamples.Adapter.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatternsSamples.Adapter
{
    internal class Traveler
    {
        internal void Travel(ITransport transport)
        {
            transport.Drive();
        }
    }
}
