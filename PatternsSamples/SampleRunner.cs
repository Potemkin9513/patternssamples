﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatternsSamples
{
    internal abstract class SampleRunner
    {
        internal abstract string PatternName { get; }

        internal abstract void Run();
    }
}
